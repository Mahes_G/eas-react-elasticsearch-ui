import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import API from "../service/API";

export default class SubmitSelJobsPopUp extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.consolidatedData = {
      firstName: "",
      lastName: "",
      emailVal: "",
      selListOfJobs: props.selListOfJobs
    };
  }

  handleClick = () => {
    this.props.toggle();
  };

  uploadResume = () => {
    console.log("In uploadResume(): ");
  };

  handleInput = event => {
    this.consolidatedData[event.target.id] = event.target.value;
  };

  async submitJob() {
    try {
      const response = await API.post("/sendEmail", this.consolidatedData);
      console.log("Returned data:", response);
    } catch (e) {
      console.log(`Axios request failed: ${e}`);
    }
  }

  render() {
    return (
      <div className="modal">
        <div className="modal_content">
          <span className="modal-popup-title">{this.props.title}</span>
          <span className="modal-popup-close" onClick={this.handleClick}>
            &times;
          </span>
          <table className="modal-popup-form-sub-content">
            <tbody>
              <tr>
                <td>
                  <Button variant="contained" size="small" onClick={() => this.uploadResume()}>
                    Upload your RESUME
                  </Button>
                </td>
              </tr>
              <tr>
                <td>
                  <TextField label="First name" variant="outlined"
                    id="firstName"
                    name="firstName"
                    onChange={this.handleInput}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <TextField label="Last name" variant="outlined"
                    id="lastName"
                    name="lastName"
                    onChange={this.handleInput}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <TextField label="Email" variant="outlined"
                    id="emailVal"
                    name="emailVal"
                    onChange={this.handleInput}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <Button variant="contained" color="secondary" onClick={() => this.handleClick()}>Cancel</Button>
                  <Button variant="contained" color="primary" onClick={() => this.submitJob()}>Submit</Button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
