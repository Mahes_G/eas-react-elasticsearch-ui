import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:9200/test-recruiter",
  responseType: "json"
});
