import React from "react";
import Button from '@material-ui/core/Button';
import Autosuggest from "react-autosuggest";
import AdditionalSearch from "./AdditionalSearch";
import "../../assets/Autosuggest.css";
import "./AutosuggestSearchApp.css";
import API from "../../service/API";

let uniqueJobLocations = [];
let uniqueJobSkills = [];
let jobDetails = [];

function getSuggestionsForSkill(value) {
  const escapedValue = value.trim();

  if (escapedValue === "") {
    return [];
  }

  return uniqueJobSkills.filter(uniJobSkill =>
    uniJobSkill.unique_skill.toLowerCase().includes(escapedValue.toLowerCase())
  );
}

function getSuggestionsForLoc(value) {
  const escapedValue = value.trim();

  if (escapedValue === "") {
    return [];
  }

  return uniqueJobLocations.filter(uniJobLoca =>
    uniJobLoca.unique_location.toLowerCase().includes(escapedValue.toLowerCase())
  );
}

function getSuggestionValueForSkill(suggestion) {
  return suggestion.unique_skill;
}

function getSuggestionValueForLoc(suggestion) {
  return suggestion.unique_location;
}

function renderSuggestionForSkill(suggestion) {
  return <span>{suggestion.unique_skill}</span>;
}

function renderSuggestionForLoc(suggestion) {
  return <span>{suggestion.unique_location}</span>;
}

class AutosuggestSearchApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      skillSearchVal: "",
      suggestionsForSkill: [],
      locSearchVal: "",
      suggestionsForLoc: [],
      loadedApiData: false,
      searchOrResetButtonMode: 0 // 'searchOrResetButtonMode' is 0 for "Search" and 1 for "Reset"
    };

    this.autoSearchUI = <p>Loading...</p>;
  }

  convertLocSetToArray = (val1, val2, setItself) => {
    uniqueJobLocations.push({ unique_location: val1 });
  };

  loadUniqueJobLoc = () => {
    // Loading unique locations to avoid displaying duplicate search results
    uniqueJobLocations = [];
    let uniqueSet = new Set();
    jobDetails.map((jobData, key) => uniqueSet.add(jobData.area_serving));
    uniqueSet.forEach(this.convertLocSetToArray);
  };

  convertSkillSetToArray = (val1, val2, setItself) => {
    uniqueJobSkills.push({ unique_skill: val1 });
  };

  loadUniqueJobSkill = () => {
    // Loading unique Skills from Primary and Secondary Skills to avoid displaying duplicate search results
    uniqueJobSkills = [];
    let uniqueSet = new Set();
    jobDetails.map((jobData, key) => {
      uniqueSet.add(jobData.primary_skill);
      uniqueSet.add(jobData.secondary_skill);
      return null;
    });
    uniqueSet.forEach(this.convertSkillSetToArray);
  };

  async componentDidMount() {
    let userData;
    try {
      userData = await API.get("/_search");
      if (
        userData !== undefined &&
        userData !== null &&
        userData.data !== undefined
      ) {
        jobDetails = userData.data.hits.hits.map((jobData, key) => {
          return {
            "jobId": jobData._id,
            "primary_skill": jobData._source.primary_skill,
            "secondary_skill": jobData._source.secondary_skill,
            "area_serving": jobData._source.area_serving,
            "recruiter_name": jobData._source.recruiter_name,
            "serving_nationalwide": jobData._source.serving_nationalwide,
            "recriter_id": jobData._source.recriter_id,
            "recuiter_phone_number": jobData._source.recuiter_phone_number,
            "creation_date": jobData._source.creation_date,
            "recruiter_email": jobData._source.recruiter_email
          };
        })
        this.loadUniqueJobLoc();
        this.loadUniqueJobSkill();
        this.setState(() => ({ loadedApiData: true }));
      }
    } catch (e) {
      console.log(`Axios request failed: ${e}`);
    }
  }

  onChangeForSkill = (event, { newValue, method }) => {
    this.setState({
      skillSearchVal: newValue
    });
  };

  onChangeForLoc = (event, { newValue, method }) => {
    this.setState({
      locSearchVal: newValue
    });
  };

  onSuggestionsFetchRequestedForSkill = ({ value }) => {
    this.setState({
      suggestionsForSkill: getSuggestionsForSkill(value)
    });
  };

  onSuggestionsFetchRequestedForLoc = ({ value }) => {
    this.setState({
      suggestionsForLoc: getSuggestionsForLoc(value)
    });
  };

  onSuggestionsClearRequestedForSkill = () => {
    this.setState({
      suggestionsForSkill: []
    });
  };

  onSuggestionsClearRequestedForLoc = () => {
    this.setState({
      suggestionsForLoc: []
    });
  };

  updateAvailabilityOfAutoSearchInputFields = flag => {
    // The following piece of code is necessary to overide the 3rd party autosearch component
    document
      .getElementById("autoSuggestDivForSkill")
      .getElementsByTagName("input")[0].disabled = flag;
    document
      .getElementById("autoSuggestDivForLoc")
      .getElementsByTagName("input")[0].disabled = flag;
  };

  showAdditionalSearchComp = () => {
    // 'searchOrResetButtonMode' is 0 for "Search" and 1 for "Reset"
    if (this.state.searchOrResetButtonMode === 0) {
      this.updateAvailabilityOfAutoSearchInputFields(true);
      this.additionalSearchComp = (
        <AdditionalSearch
          skillSearchVal={this.state.skillSearchVal}
          locSearchVal={this.state.locSearchVal}
          uniqueJobLocations={uniqueJobLocations}
          jobDetails={jobDetails}
        />
      );
    } else {
      this.updateAvailabilityOfAutoSearchInputFields(false);
      this.additionalSearchComp = <></>;
    }
    this.setState({
      searchOrResetButtonMode: this.state.searchOrResetButtonMode === 0 ? 1 : 0
    });
  };

  render() {
    const {
      skillSearchVal,
      suggestionsForSkill,
      locSearchVal,
      suggestionsForLoc
    } = this.state;

    const inputPropsForSkill = {
      placeholder: "Enter Skill (Partial Match)",
      value: skillSearchVal,
      onChange: this.onChangeForSkill
    };

    const inputPropsForLoc = {
      placeholder: "Enter Job Location (Exact Match)",
      value: locSearchVal,
      onChange: this.onChangeForLoc
    };

    if (this.state.loadedApiData) {
      this.autoSearchUI = (
        <>
          <div id="autoSuggestDivForSkill" className="eas-app-block">
            <Autosuggest
              suggestions={suggestionsForSkill}
              onSuggestionsFetchRequested={
                this.onSuggestionsFetchRequestedForSkill
              }
              onSuggestionsClearRequested={
                this.onSuggestionsClearRequestedForSkill
              }
              getSuggestionValue={getSuggestionValueForSkill}
              renderSuggestion={renderSuggestionForSkill}
              inputProps={inputPropsForSkill}
              id="auto-suggest-skill"
            />
          </div>
          <div id="autoSuggestDivForLoc" className="eas-app-block">
            <Autosuggest
              suggestions={suggestionsForLoc}
              onSuggestionsFetchRequested={
                this.onSuggestionsFetchRequestedForLoc
              }
              onSuggestionsClearRequested={
                this.onSuggestionsClearRequestedForLoc
              }
              getSuggestionValue={getSuggestionValueForLoc}
              renderSuggestion={renderSuggestionForLoc}
              inputProps={inputPropsForLoc}
              id="auto-suggest-location"
            />
          </div>
          <div className="eas-app-block">
            <Button variant="contained" color={this.state.searchOrResetButtonMode === 0 ? "primary" : "secondary"} size="large" onClick={() => this.showAdditionalSearchComp()}>
              {this.state.searchOrResetButtonMode === 0 ? "Search" : "Reset"}
            </Button>
          </div>
        </>
      );
    }

    return (
      <div>
        {this.autoSearchUI}
        {this.additionalSearchComp}
      </div>
    );
  }
}

export default AutosuggestSearchApp;
