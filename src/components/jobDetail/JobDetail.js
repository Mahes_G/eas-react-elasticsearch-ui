import React, { Component } from "react";
import Checkbox from '@material-ui/core/Checkbox';
import JobDetailPopUp from "../../util/JobDetailPopUp";

class JobDetail extends Component {
  constructor(props) {
    super(props);
    this.state = { displayPopUp: false, jobSelected: false };
    this.jobDetail = props.jobDetail;
    this.updateSelectedJobs = props.updateSelectedJobs;
  }

  togglePopUp = () => {
    this.setState(() => ({ displayPopUp: !this.state.displayPopUp }));
  };

  updateSelectedJob = event => {
    if (event.target.checked) {
      this.setState(() => ({ jobSelected: true }));
    } else {
      this.setState(() => ({ jobSelected: false }));
    }
    this.updateSelectedJobs(event, this.jobDetail);
  };

  render() {
    this.detailedJobInfo = (
      <table className="eas-app-detail-table">
        <tbody>
          <tr>
            <td className="eas-app-detail-table-title">Recruiter Name: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.recruiter_name} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Serving Nationalwide: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.serving_nationalwide + " "} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Primary skill: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.primary_skill} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Area Serving: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.area_serving} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Recuiter Phone Number: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.recuiter_phone_number} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Recruiter email: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.recruiter_email} </td>
          </tr>
        </tbody>
      </table>
    );

    return (
      <div
        className={
          "eas-app-job-content eas-inline-block" +
          (this.state.jobSelected ? " eas-selected-job" : "")
        }
      >
        <div className="eas-inline-block">
          <Checkbox
            color="default"
            id={"job_chk_box_" + this.jobDetail.jobId}
            name={"job_chk_box_" + this.jobDetail.jobId}
            onClick={e => this.updateSelectedJob(e)}
          />
        </div>
        <div className="eas-inline-block" onClick={this.togglePopUp}>
        {this.jobDetail.recruiter_name}
        </div>
        {this.state.displayPopUp ? (
          <JobDetailPopUp
            title="Recruiter Detail"
            content={this.detailedJobInfo}
            toggle={this.togglePopUp}
          />
        ) : null}
      </div>
    );
  }
}

export default JobDetail;
